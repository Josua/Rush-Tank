#include	"tank.h"

ALuint	ground_music_buffer;
ALuint	ground_music_source;
ALuint	missile_buffer;
ALuint	missile_source;
ALuint	explode_buffer;
ALuint	explode_source;

void
load_voices()
{
	missile_buffer = alutCreateBufferFromFile("missile.wav");
	alGenSources(1, &missile_source);
	explode_buffer = alutCreateBufferFromFile("explode.wav");
	alGenSources(1, &explode_source);
	return;
}

void
unload_voices()
{
	alDeleteBuffers(1, &ground_music_buffer);
	alDeleteSources(1, &ground_music_source);
	alDeleteBuffers(1, &missile_buffer);
	alDeleteSources(1, &missile_source);
	alDeleteBuffers(1, &explode_buffer);
	alDeleteBuffers(1, &explode_source);
	alutExit();
	_exit(0);
	return;
}

void
play_missile()
{
	alSourcei(missile_source, AL_BUFFER, missile_buffer);
	alSourcei(missile_source, AL_LOOPING, AL_FALSE);
	alSourcePlay(missile_source);
	return;
}

void
play_explode()
{
	static int hit_flag = 0;
	if(!((tank0.skill_flag & MISSILE_HIT) || (tank0.skill_flag & BOMB_HIT))) {
		hit_flag = 0;
		return;
	}
	if(hit_flag)
		return;
	hit_flag = 1;
	alSourcei(explode_source, AL_BUFFER, explode_buffer);
	alSourcei(explode_source, AL_LOOPING, AL_FALSE);
	alSourcePlay(explode_source);
	return;
}

void
play_ground_music()
{
	ground_music_buffer = alutCreateBufferFromFile("ground_music.wav");
	alGenSources(1, &ground_music_source);
        alSourcei(ground_music_source, AL_BUFFER, ground_music_buffer);
	alSourcei(ground_music_source, AL_LOOPING, AL_TRUE);
	alSourcePlay(ground_music_source);
	return;
}	
