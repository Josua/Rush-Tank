#ifndef		SHOW_H
#define		SHOW_H

extern time_t run_time;
extern time_t last_run_time;
extern time_t all_time;

void 	show_tank(void *);
void 	draw_arrow(void);
void 	show_speed(void);
void 	show_time(void);
void 	show_sort(void);
void 	show_wait(void);
void 	show_final_sort(void);
void	show_number(unsigned int, float, float);
void 	rand_trees_and_grasses(void);
void 	show_trees_and_grasses(void);
void 	show_skill_box(void);
void 	use_skill(int);
void 	show_item(float, int);
int  	show_welcom(void);
void	show_player_num(void);

#endif					/* SHOW_H */
