#ifndef		NET_H
#define		NET_H
#define		MAX_ITEM_NUM	100

enum Net {
RESERVE, NEW_TANK, GET_TANKS, GET_MAP, GET_TIME, GET_ITEMS, ADD_SPEED, SKILL, READY, PAUSE
};

extern int tanks_count;

void 	net_init(void);
int 	take_part(void);
void 	get_tanks(void);
void 	send_req(enum Net);
void 	net_get(void *, int);
void 	show_tanks(void);
void	show_items(void);
void	get_items(void);
void	show_minimap(void);
void 	add_speed(float);
void 	get_map(void);

#endif				/* NET_H */
