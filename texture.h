#ifndef		TEXTURE
#define		TEXTURE

#define		SKY_TEX_NUM		5      
#define         GROUND_TEX_NUM      	5
#define         TANK_TEX_NUM        	5
#define		GRASS_TEX_NUM		5
#define		UPGRASS_TEX_NUM		5
#define		BOMB_HIT_TEX_NUM	5
#define		MISSILE_HIT_TEX_NUM	5
#define		SPRINT_HIT_TEX_NUM	5

extern GLuint          	groundtex[];
extern GLuint          	tanktex[];
extern GLuint          	skytex[];
extern GLuint          	grasstex[];
extern GLuint		upgrasstex[];
extern GLuint		bomb_hit_tex[];
extern GLuint		missile_hit_tex[];
extern GLuint		sprint_hit_tex[];

extern int		skytex_count;
extern int		groundtex_count;
extern int		tanktex_count;
extern int		grasstex_count;
extern int		upgrasstex_count;
extern int		bomb_hit_tex_count;
extern int		missile_hit_tex_count;
extern int		sprint_hit_tex_count;
	
extern GLuint		sky_tex;
extern GLuint		ground_tex;
extern GLuint		grass_tex;
extern GLuint		start_tex;
extern GLuint		end_tex;
extern GLuint		wait_tex;
extern GLuint		sort_tex;
extern GLuint		tree_tex;
extern GLuint           bomb_table_tex;
extern GLuint           sprint_table_tex;
extern GLuint           missile_table_tex;
extern GLuint	        shield_table_tex;
extern GLuint		shield_hit_tex;
extern GLuint		missile_tex;
extern GLuint		bomb_tex;
extern GLuint		welcom_tex;

float 	loadtextures(void);
void 	choosetex(GLuint);
void 	rand_texture(void);
void 	load_welcom_tex(void);

#endif				/* TEXTURE */
