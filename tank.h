#ifndef		TANK_H
#define		TANK_H
#define		_CRT_SECURE_NO_WARNINGS

#include	<unistd.h>
#include	<GL/glut.h>
#include        <time.h>
#include        <math.h>
#include        <time.h>
#include        <stdio.h>
#include        <stdlib.h>
#include	<string.h>
#include        "keyboard.h"
#include        "check.h"
#include        "map.h"
#include        "texture.h"
#include	"renew.h"
#include	"net.h"
#include	"calc.h"
#include        "show.h"
#include	"voice.h"

#define		MISSILE_HIT	0x0001
#define		BOMB_HIT	0x0002
#define		SPRINT_HIT	0x0004
#define		SHIELD_HIT	0x0008
#define         MAX_VERTEX      500
#define		TANK_L		0.2

#ifdef		_WIN32
#define		CLOCK_REALTIME	0		
struct timespec {
long 	tv_sec;            /* Seconds.  */
long 	tv_nsec;  /* Nanoseconds.  */
};
extern int clock_gettime(int __clock_id, struct timespec *__tp);
#endif				/* _WIN32 */

enum    Skill {
MISSILE, BOMB, SPRINT, SHIELD
};

struct Skill_state {
enum	Skill	skill_name;
int		use_time;
int		cool_time;
};

struct Tank {
int			id;
int			sort;
float 			posx;
float 			posy;
double 			angle;
float 			base_speed;
int			skill_flag;
};

extern int		check_count;
extern int		vertex_count;
extern int		wait_flag;
extern int		net_flag;
extern int 		check_flag;
extern int		start_flag;
extern int		eye_flag;
extern float		eye_x;
extern float		eye_y;
extern char		check_str[];
extern struct Tank	tank0;
extern GLfloat		arrow_color[][3];
extern struct timespec	start_time;
extern struct timespec	cur_time;
extern float		mapvertex[];
extern struct Skill_state      skills[];

void tank_reload(void);

#endif				/* TANK_H */
