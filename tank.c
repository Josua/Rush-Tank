#include	"tank.h"
#define		SPEED_FACTOR	10000000000

struct Tank 	tank0 = { 0 };
int 		check_flag = 0;			
int 		check_count = 0;
int		vertex_count = 0;
int		wait_flag = 0;
int		start_flag = 0;
int		net_flag = 1;
int		eye_flag = 1;
float 		eye_x = 0;
float		eye_y = 0;
char		check_str[MAX_STRLEN] = { 0 };
float 		mapvertex[MAX_VERTEX] = { 0.0 };
GLfloat		arrow_color[MAX_STRLEN][3] = { { 1.0, 0.0, 0.0 }, { 1.0, 0.0, 0.0 }, { 1.0, 0.0, 0.0 }, { 1.0, 0.0, 0.0 }, { 1.0, 0.0, 0.0 }, \
						{ 1.0, 0.0, 0.0 }, { 1.0, 0.0, 0.0 }, { 1.0, 0.0, 0.0 }, { 1.0, 0.0, 0.0 }, { 1.0, 0.0, 0.0 } };
struct timespec 	start_time; 
struct timespec	 	cur_time;
struct timespec 	last_time;
struct Skill_state	skills[SHIELD+1] = { { MISSILE, 0, 15 }, { BOMB, 0, 10 }, { SPRINT, 0, 20 }, { SHIELD, 0, 12 } };

#ifdef _WIN32
int
clock_gettime(int __clock_id, struct timespec * __tp)
{
	__tp->tv_sec = time(NULL);
	__tp->tv_nsec = clock() * 1E6;
	return 0;
}
#endif          /* _WIN32 */

void
tank_reload()
{
	int i;
	for(i = 0; i < SHIELD + 1; i++) {
		skills[i].use_time = 0;
	}
	clock_gettime(CLOCK_REALTIME, &start_time);
        cur_time = start_time;
        last_time = start_time;
	run_time = 0;
	last_run_time = 0;
	memset(&tank0, 0, sizeof(tank0));
	start_flag = 0;
	check_flag = 0;
	rand_texture();
        if(net_flag) {
                if(take_part()) {
			wait_flag = 0;
		} else {
			wait_flag = 1;
		}
		get_map();
		load_node();
		pre_process();
               	get_tanks();
        } else {
		load_map(); 
	}
	rand_trees_and_grasses();
	return; 
}

void
tank_init()
{
	play_ground_music();
	net_init();
	srand((unsigned)time(NULL));
	glEnable(GL_TEXTURE_2D);
        glEnable(GL_ALPHA_TEST);
        glAlphaFunc(GL_GREATER, 0.5);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glClearColor(1.0, 1.0, 1.0, 0.0);
        glShadeModel(GL_FLAT);
	load_welcom_tex();
	return;
}

void
tank_display(void)
{
	if(show_welcom()) {	
		glutSwapBuffers();
		return;
	}
	glClear(GL_COLOR_BUFFER_BIT);
	draw_map(vertex_count, mapvertex);
	show_trees_and_grasses();
	if(tank0.posx > mapvertex[vertex_count - 4])
		show_final_sort();
	if(net_flag) {
		show_tanks();
		show_items();
		play_explode();
	} else {
		show_tank(&tank0);
	}
	if(eye_flag) {
		show_minimap();
		show_speed();
		show_time();
		show_player_num();
		if(net_flag) {
			show_skill_box();
			show_sort();
		}
	}
	if(check_flag)  
		draw_arrow();
	glutSwapBuffers();
	return;
}
	
void
tank_reshape(int w, int h)
{
	glViewport(0, 0, (GLsizei)w, (GLsizei)h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, (GLdouble)w, 0.0, (GLdouble)h, 0.0, 0.0);
	return;
}

void
tank_run()
{
	int i;	
	if (show_welcom()) {
		glutSwapBuffers();
		return;
	}
	if(tank0.posx > mapvertex[vertex_count - 4]) {
		start_flag = 0;
		check_flag = 0;
	}
	if(net_flag) {
		get_tanks();
		get_items();
	} else {
		if(start_flag) {
			clock_gettime(CLOCK_REALTIME, &cur_time);
			last_time = cur_time;
			tank0.posx += 0.001 * (tank0.base_speed - tan(tank0.angle / 180.0 * 3.14159));
			tank_pos(TANK_L, tank0.posx, &tank0.posy, &tank0.angle);
		}
	}
	if(check_flag == 0 && eye_flag && start_flag && !wait_flag) {
		i = rand() % 10;
		if(i > 0 && i < 10)	
			make_check(i);
	}
	if(eye_flag) {
		eye_x = tank0.posx;
		eye_y = tank0.posy + 0.2;
	}
	glutPostRedisplay();
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
       	gluLookAt(eye_x, eye_y, 1.0, eye_x, eye_y, 0.0, 0.0, 1.0, 0.0);
	return;
}	
	
int
main(int argc, char *argv[])
{
	alutInit(&argc, argv);
	load_voices();
	atexit(unload_voices);
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
        glutInitWindowPosition(100, 100);
        glutInitWindowSize(1000, 618);
        glutCreateWindow(argv[0]);
	tank_init();
	glutDisplayFunc(tank_display);
	glutReshapeFunc(tank_reshape);
        glutKeyboardFunc(keyboard);
        glutSpecialFunc(specialkeyboard);
	glutIdleFunc(tank_run);
        glutMainLoop();
        return 0;
}

