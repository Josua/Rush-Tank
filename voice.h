#ifndef		VOICE_H
#define		VOICE_H
#include	<AL/alut.h>
#define		MAX_VOICE_NUM	100

void play_ground_music(void);
void load_voices(void);
void unload_voices(void);
void play_missile(void);
void play_explode(void);

#endif			/* VOICE_H */
