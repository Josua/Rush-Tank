#include	"tank.h"

GLuint          skytex[SKY_TEX_NUM];
GLuint		groundtex[GROUND_TEX_NUM];
GLuint		tanktex[TANK_TEX_NUM];
GLuint		grasstex[GRASS_TEX_NUM];
GLuint		upgrasstex[UPGRASS_TEX_NUM];
GLuint		sprint_hit_tex[SPRINT_HIT_TEX_NUM];
GLuint		bomb_hit_tex[BOMB_HIT_TEX_NUM];
GLuint		missile_hit_tex[MISSILE_HIT_TEX_NUM];

int		skytex_count;
int		groundtex_count;
int		tanktex_count;
int		grasstex_count;
int		upgrasstex_count;
int		sprint_hit_tex_count;
int		bomb_hit_tex_count;
int		missile_hit_tex_count;

GLuint		sky_tex;
GLuint		ground_tex;
GLuint		grass_tex;
GLuint		start_tex;
GLuint		end_tex;
GLuint		wait_tex;
GLuint		sort_tex;
GLuint		tree_tex;
GLuint		welcom_tex;

GLuint		bomb_table_tex;
GLuint		sprint_table_tex;
GLuint		missile_table_tex;
GLuint		shield_table_tex;
GLuint		shield_hit_tex;
GLuint		bomb_tex;
GLuint		missile_tex;

void
rand_texture()
{
	sky_tex = skytex[rand()%skytex_count];
	ground_tex = groundtex[rand()%groundtex_count];
	grass_tex = grasstex[rand()%grasstex_count];
	return;
}

void
tank_itoa(int num, char str[])
{
	switch(num) {
		case 0: str[0] = '0'; break;
		case 1: str[0] = '1'; break;
		case 2: str[0] = '2'; break;
		case 3: str[0] = '3'; break;
		case 4: str[0] = '4'; break;
		case 5: str[0] = '5'; break;
		case 6: str[0] = '6'; break;
		case 7: str[0] = '7'; break;
		case 8: str[0] = '8'; break;
		case 9: str[0] = '9'; break;
	}
	str[1] = 0;
	return;
}

void
adjustpixel(GLubyte *pixeldata, int pos, int offset, int num, int width)
{
        int i;
        if(num < width * 3)
                return;
        for(i = 0; i < num; i++) {
                pixeldata[pos-offset+i] = pixeldata[pos+i];
        }
        pos += width * 3;
        num -= width * 3 + offset;
        adjustpixel(pixeldata, pos, offset, num, width);
        return;
}

void
bindtex(GLuint texName, GLubyte * tex, int width, int height)
{
	glBindTexture(GL_TEXTURE_2D, texName);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, tex);
	return;
}

GLubyte *
loadbmp(char *bmpname, int *imagewidth, int *imageheight)
{
	GLubyte *tex, *pixeldata;
        int  x, y, pixellength;
        int  offset;
	FILE *pfile;
	pfile = fopen(bmpname, "rb");
        if(pfile == NULL)
		return NULL;
        fseek(pfile, 0x0012, SEEK_SET);
        fread(imagewidth, sizeof(*imagewidth), 1, pfile);
        fread(imageheight, sizeof(*imageheight), 1, pfile);
       	pixellength = (*imagewidth) * 3;
        while(pixellength % 4 != 0) pixellength++;
        offset = pixellength - (*imagewidth) * 3;
        pixellength *= (*imageheight);
        pixeldata = (GLubyte *)malloc(pixellength);
        if(pixeldata == NULL) exit(0);
        fseek(pfile, 54, SEEK_SET);
        fread(pixeldata, pixellength, 1, pfile);
        fclose(pfile);
	
	adjustpixel(pixeldata, (*imagewidth) * 3  + offset, offset, pixellength - ((*imagewidth) * 3 + offset), (*imagewidth));
	tex = (GLubyte *)malloc((*imagewidth) * (*imageheight) * 4);
        if(tex == 0) exit(0);
        for(x = 0, y = 0; x < ((*imagewidth)*(*imageheight)*3); x += 3, y += 4) {
                tex[y] = pixeldata[x+2];
                tex[y+1] = pixeldata[x+1];
                tex[y+2] = pixeldata[x];
                if(pixeldata[x] == 255 && pixeldata[x+1] == 255 && pixeldata[x+2] == 255) {
                        tex[y+3] = 0;
                } else {
                        tex[y+3] = 255;
                }
        }
	return tex;
}

float
loadtextures()
{
	static float process = 0.0;
	int 	i = 0;
	int 	width, height;
	GLubyte *tex;
	char tmp[3] = { 0 };
	char bmpname[20] = { 0 };
	if(process < 0.05) {
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
       	 	glGenTextures(SKY_TEX_NUM, skytex);
 		glGenTextures(GROUND_TEX_NUM, groundtex);
        	glGenTextures(TANK_TEX_NUM, tanktex);
		glGenTextures(GRASS_TEX_NUM, grasstex);
		glGenTextures(UPGRASS_TEX_NUM, upgrasstex);
		glGenTextures(BOMB_HIT_TEX_NUM, bomb_hit_tex);
		glGenTextures(SPRINT_HIT_TEX_NUM, sprint_hit_tex);
		glGenTextures(MISSILE_HIT_TEX_NUM, missile_hit_tex);
		glGenTextures(1, &start_tex);
		glGenTextures(1, &end_tex);
		process = 0.06;
		return process;
	}
	if(process < 0.10) {
		glGenTextures(1, &wait_tex);
		glGenTextures(1, &sort_tex);
		glGenTextures(1, &tree_tex);
		glGenTextures(1, &bomb_table_tex);
		glGenTextures(1, &missile_table_tex);
		glGenTextures(1, &sprint_table_tex);
		glGenTextures(1, &shield_table_tex);
		glGenTextures(1, &shield_hit_tex);
		glGenTextures(1, &bomb_tex);
		glGenTextures(1, &missile_tex);
		process = 0.11;
		return process;
	}
	if(process < 0.20) {
		for(i = 0; i < SKY_TEX_NUM; i++) {
			bmpname[0] = 0;
			strcat(bmpname, "sky");
			tank_itoa(i, tmp);
			strcat(bmpname, tmp);
			strcat(bmpname, ".bmp");
			if((tex = loadbmp(bmpname, &width, &height)) == NULL) {
				break;
			}
			bindtex(skytex[i], tex, width, height);
		}
		skytex_count = i;
		process = 0.21;
		return process;
	}
	if(process < 0.30) {
		for(i = 0; i < GROUND_TEX_NUM; i++) {
			bmpname[0] = 0;
               	 	strcat(bmpname, "ground");
                	tank_itoa(i, tmp);
                	strcat(bmpname, tmp);
                	strcat(bmpname, ".bmp");
                	if((tex = loadbmp(bmpname, &width, &height)) == NULL) {
                        	break;
			}
			bindtex(groundtex[i], tex, width, height);
		}
		groundtex_count = i;
		process = 0.31;
		return process;
	}
	if(process < 0.35) {
		for(i = 0; i < GRASS_TEX_NUM; i++) {
                	bmpname[0] = 0;
                	strcat(bmpname, "grass");
                	tank_itoa(i, tmp);
               	 	strcat(bmpname, tmp);
                	strcat(bmpname, ".bmp");
                	if((tex = loadbmp(bmpname, &width, &height)) == NULL) {
                        	break;
                	}
                	bindtex(grasstex[i], tex, width, height);
        	}
        	grasstex_count = i;
		process = 0.36;
		return process;
	}
	if(process < 0.40) {
		for(i = 0; i < TANK_TEX_NUM; i++) {
                	bmpname[0] = 0;
                	strcat(bmpname, "tank");
               		tank_itoa(i, tmp);
                	strcat(bmpname, tmp);
                	strcat(bmpname, ".bmp");
                	if((tex = loadbmp(bmpname, &width, &height)) == NULL) {
                        	break;
                	}
                	bindtex(tanktex[i], tex, width, height);
        	}
        	tanktex_count = i;
		process = 0.41;
		return process;
	}
	if(process < 0.45) {
		for(i = 0; i < UPGRASS_TEX_NUM; i++) {
	        	bmpname[0] = 0;
                	strcat(bmpname, "upgrass");
                	tank_itoa(i, tmp);
                	strcat(bmpname, tmp);
                	strcat(bmpname, ".bmp");
                	if((tex = loadbmp(bmpname, &width, &height)) == NULL) {
                        	break;
                	}	
                	bindtex(upgrasstex[i], tex, width, height);
		}
		upgrasstex_count = i;
		process = 0.46;
		return process;
	}
	if(process < 0.55) {
		for(i = 0; i < BOMB_HIT_TEX_NUM; i++) {
                	bmpname[0] = 0;
                	strcat(bmpname, "bomb_hit");
                	tank_itoa(i, tmp);
                	strcat(bmpname, tmp);
                	strcat(bmpname, ".bmp");
                	if((tex = loadbmp(bmpname, &width, &height)) == NULL) {
                        	break;
                	}
               		bindtex(bomb_hit_tex[i], tex, width, height);
        	}
        	bomb_hit_tex_count = i;
		process = 0.56;
		return process;
	}
	if(process < 0.65) {
		for(i = 0; i < MISSILE_HIT_TEX_NUM; i++) {
                	bmpname[0] = 0;
                	strcat(bmpname, "missile_hit");
               	 	tank_itoa(i, tmp);
                	strcat(bmpname, tmp);
                	strcat(bmpname, ".bmp");
                	if((tex = loadbmp(bmpname, &width, &height)) == NULL) {
                        	break;
                	}
                	bindtex(missile_hit_tex[i], tex, width, height);
        	}
        	missile_hit_tex_count = i;
		process = 0.66;
		return process;
	}
	if(process < 0.75) {
		for(i = 0; i < SPRINT_HIT_TEX_NUM; i++) {
                	bmpname[0] = 0;
                	strcat(bmpname, "sprint_hit");
                	tank_itoa(i, tmp);
                	strcat(bmpname, tmp);
                	strcat(bmpname, ".bmp");
                	if((tex = loadbmp(bmpname, &width, &height)) == NULL) {
                        	break;
                	}
               	 	bindtex(sprint_hit_tex[i], tex, width, height);
        	}
        	sprint_hit_tex_count = i;
		process = 0.76;
		return process;
	}
	if(process < 0.80) {
		tex = loadbmp("start.bmp", &width, &height);
		bindtex(start_tex, tex, width, height);
		tex = loadbmp("end.bmp", &width, &height);
		bindtex(end_tex, tex, width, height);
		process = 0.81;
		return process;
	}
	if(process < 0.82) {
		tex = loadbmp("wait.bmp", &width, &height);
		bindtex(wait_tex, tex, width, height);
		tex = loadbmp("sort.bmp", &width, &height);
		bindtex(sort_tex, tex, width, height);
		process = 0.83;
		return process;
	}
	if(process < 0.84) {
		tex = loadbmp("tree.bmp", &width, &height);
		bindtex(tree_tex, tex, width, height);
		tex = loadbmp("bomb_table.bmp", &width, &height);
		bindtex(bomb_table_tex, tex, width, height);
		process = 0.85;
		return process;
	}
	if(process < 0.86) {
        	tex = loadbmp("sprint_table.bmp", &width, &height);
       	 	bindtex(sprint_table_tex, tex, width, height);
       	 	tex = loadbmp("missile_table.bmp", &width, &height);
        	bindtex(missile_table_tex, tex, width, height);
		process = 0.87;
		return process;
	}
	if(process < 0.88) {
	        tex = loadbmp("shield_table.bmp", &width, &height);
        	bindtex(shield_table_tex, tex, width, height);
        	tex = loadbmp("shield_hit.bmp", &width, &height);
        	bindtex(shield_hit_tex, tex, width, height);
		process = 0.89;
		return process;
	}
	if(process < 0.90) {
		tex = loadbmp("bomb.bmp", &width, &height);
		bindtex(bomb_tex, tex, width, height);
		tex = loadbmp("missile.bmp", &width, &height);
		bindtex(missile_tex, tex, width, height);
		process = 0.91;
		return process;
	}
	if(process < 0.95) {
		tank_reload();
		process = 0.96;
		return process;
	}
	return 1.0;
}

void
load_welcom_tex()
{
	int width, height;
	GLubyte *tex;
	glGenTextures(1, &welcom_tex);
	tex = loadbmp("welcom.bmp", &width, &height);
	bindtex(welcom_tex, tex, width, height);
	return;
}

void
choosetex(GLuint texName)
{
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
        glBindTexture(GL_TEXTURE_2D, texName);
	return;
}
	
