#include	"tank.h"
#define		EYE_SMALL_STEP		0.05
#define		EYE_BIG_STEP		1.0
void
keyboard(unsigned char key, int x, int y)
{
        switch(key) {
		case 'R':
		case 'r':
			if(net_flag)
				send_req(READY);
			start_flag = 1;
			break;
		case 'P':
		case 'p':
			if(net_flag)
				send_req(PAUSE);
			start_flag = 0;
			check_flag = 0;
			break;
		case 'G':
		case 'g':
			if(tank0.posx > mapvertex[vertex_count-4])
				tank_reload();
			break;
		case 'W':
		case 'w':
			if(eye_flag) break;
			eye_y += EYE_BIG_STEP;
			break;
		case 'S':
		case 's':
			if(eye_flag) break;
			eye_y -= EYE_BIG_STEP;
			break;
		case 'A':
		case 'a':
			if(eye_flag) break;
			eye_x -= EYE_BIG_STEP;
			break;
		case 'D':
		case 'd':
			if(eye_flag) break;
			eye_x += EYE_BIG_STEP;
			break;
		case '1':
			use_skill(0);
			break;
		case '2':
			use_skill(1);
			break;
		case '3':
			use_skill(2);
			break;
		case '4':
			use_skill(3);
			break;
		case 'Y':
		case 'y':
			eye_flag ^= 1;
			check_flag = 0;
			break;
		}
	return;
}

void
specialkeyboard(int key, int x, int y)
{	
        switch(key) {
                case GLUT_KEY_LEFT:
			if(eye_flag) {
                        	check('l');
			} else {
				eye_x -= EYE_SMALL_STEP;
			}
                        break;
                case GLUT_KEY_RIGHT:
			if(eye_flag) {
                       		check('r');
			} else {
				eye_x += EYE_SMALL_STEP;
			}
                        break;
                case GLUT_KEY_UP:
			if(eye_flag) {
                        	check('u');
			} else {
                        	eye_y += EYE_SMALL_STEP;
			}
                        break;

            case GLUT_KEY_DOWN:
			if(eye_flag) {
			        check('d');
			} else {
				eye_y -= EYE_SMALL_STEP;
			}
			break;
		}
		glutPostRedisplay();
        return;
}

