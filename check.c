#include	"tank.h"

void
make_check(int n)
{
	int i, dir;
	check_flag = 1;
	check_count = n;
	for(i = 0; i < n; i++) {
		arrow_color[i][0] = 1.0;
		arrow_color[i][1] = 0.0;
		arrow_color[i][2] = 0.0;
	}
	for(i = 0; i < n; i++) {
		dir = rand() % 4;
		switch(dir) {
			case 0: check_str[i] = 'l'; break;
			case 1: check_str[i] = 'u'; break;
			case 2: check_str[i] = 'r'; break;
			case 3: check_str[i] = 'd'; break;
		}
	}
	return;
}

void
check(char c)
{
	static int i = 0;
	int j = i;
	if(check_flag == 0)
		return;
	i++;
	if(check_str[j] == c) {
		arrow_color[j][0] = 0.0;
		arrow_color[j][1] = 1.0;
		arrow_color[j][2] = 0.0;
		if(i == check_count) {
			add_speed(i / 10.0);
			tank0.base_speed += i / 10.0;
			i = 0;
			check_flag = 0;
		}  
	} else {
		if(net_flag) {
			add_speed((-check_count) / 10.0);
		} else {
			tank0.base_speed -= check_count / 10.0;
			if(tank0.base_speed < 0.0) 
				tank0.base_speed = 0.0;
		}
		i = 0;
		check_flag = 0;
	}
	return;
}
			
