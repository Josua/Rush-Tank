#include	"tank.h"
#define		MAX_TREE_NUM 	500
#define		MAX_GRASS_NUM 	2500
#define		SKILL_BOX_POS_X	0.5
#define		SKILL_BOX_POS_Y	0.6

struct	posnod {
float	x;
float	y;
};

int		tree_count = 0;
int		grass_count = 0;
struct	posnod	treepos[MAX_TREE_NUM];
struct	posnod	grasspos[MAX_GRASS_NUM];
time_t 		run_time = 0;
time_t  	last_run_time = 0;
time_t		all_time;	

void
draw_a_quad(float left, float buttom, float right, float top)
{
        glBegin(GL_QUADS);
                glTexCoord2f(0.0, 0.0); glVertex2f(left, buttom);
                glTexCoord2f(1.0, 0.0); glVertex2f(right, buttom);
                glTexCoord2f(1.0, 1.0); glVertex2f(right, top);
                glTexCoord2f(0.0, 1.0); glVertex2f(left, top);
        glEnd();
}

void
show_arrow_left()
{
	glBegin(GL_TRIANGLES);
		glVertex2f(0.00, 0.05);
		glVertex2f(0.05, 0.00);
		glVertex2f(0.05, 0.10);
	glEnd();
	glBegin(GL_QUADS);
		glVertex2f(0.05, 0.025);
		glVertex2f(0.10, 0.025);
		glVertex2f(0.10, 0.075);
		glVertex2f(0.05, 0.075);
	glEnd();
	return;
}

void
show_arrow_down()
{
	glPushMatrix();
	glTranslatef(0.1, 0.0, 0.0);
	glRotatef(90.0, 0.0, 0.0, 1.0);
	show_arrow_left();
	glPopMatrix();
	return;
}

void
show_arrow_up()
{
	glPushMatrix();
	glTranslatef(0.0, 0.1, 0.0);
	glRotatef(-90.0, 0.0, 0.0, 1.0);
	show_arrow_left();
	glPopMatrix();
	return;
}

void
show_arrow_right()
{
	glPushMatrix();
	glTranslatef(0.1, 0.1, 0.0);
	glRotatef(180.0, 0.0, 0.0, 1.0);
	show_arrow_left();
	glPopMatrix();
	return;
}
	
void
show_arrow(char dir, float x, float y)
{
	glPushMatrix();
	glTranslatef(x, y, 0.0);
	switch(dir) {
		case 'l': show_arrow_left(); break;
		case 'r': show_arrow_right(); break;
		case 'u': show_arrow_up(); break;
		case 'd': show_arrow_down(); break;
	}
	glPopMatrix();
	return;
}

void
draw_shield_hit(float x, float y)
{
	choosetex(shield_hit_tex);
	glPushMatrix();
	glTranslatef(x + 0.05, y + 0.15, 0.0);
	draw_a_quad(0.0, 0.0, 0.1, 0.1);
	glPopMatrix();
	choosetex(0);
	return;
}

void
draw_missile_hit(float x, float y)
{
	static int i = 0;
	static float offset = 0.0;
	if(offset > 0.15)
		offset = 0.0;
	choosetex(missile_hit_tex[i/10]);
	i++;
	i %= missile_hit_tex_count * 10;
	glPushMatrix();
	glTranslatef(x - 0.05 + offset, y, 0);
	draw_a_quad(0.0, 0.0, 0.1, 0.1);
	glPopMatrix();
	offset += 0.001;
	choosetex(0);
	return;
}

void
draw_bomb_hit(float x, float y)
{
        static int i = 0;
        choosetex(bomb_hit_tex[i/10]);
        i++;
        i %= bomb_hit_tex_count * 10;
        glPushMatrix();
        glTranslatef(x - 0.05, y, 0);
        draw_a_quad(0.0, 0.0, 0.1, 0.1);
        glPopMatrix();
        choosetex(0);
        return;
}

void
draw_sprint_hit(float x, float y)
{
        static int i = 0;
        static float offset = 0.0;
        if(offset > 0.15)
                offset = 0.0;
	choosetex(sprint_hit_tex[i/10]);
        i++;
        i %= sprint_hit_tex_count * 10;
        glPushMatrix();
        glTranslatef(x - 0.05 + offset, y, 0);
        draw_a_quad(0.0, 0.0, 0.1, 0.1);
        glPopMatrix();
	offset += 0.001;
        choosetex(0);
        return;
}

void
show_tank(void *p)
{
	struct Tank *tank = (struct Tank*)p;
	static	int 	i = 0;
	choosetex(tanktex[i/5]);
	i++;
	i %= tanktex_count * 5;
	glPushMatrix();
	glTranslatef(tank->posx, tank->posy, 0.0);
	glRotatef(tank->angle, 0.0, 0.0, 1.0);
	draw_a_quad(0.0, 0.0, TANK_L, 0.1);
	glPopMatrix();
	choosetex(0);
	if(tank->skill_flag) {
		if(tank->skill_flag & MISSILE_HIT) {
			draw_missile_hit(tank->posx, tank->posy);
		}
		if(tank->skill_flag & BOMB_HIT) {
			draw_bomb_hit(tank->posx, tank->posy);
		}
		if(tank->skill_flag & SPRINT_HIT) {
			draw_sprint_hit(tank->posx, tank->posy);
		}
		if(tank->skill_flag & SHIELD_HIT) {
			draw_shield_hit(tank->posx, tank->posy);
		}
	}
	return;
}

void
show_item(float x, int what)
{
	double	angle;
	float y;
	tank_pos(0.1, x, &y, &angle);
	switch(what) {
		case BOMB: choosetex(bomb_tex); break;
		case MISSILE: choosetex(missile_tex); break;
		default: break;
	}
	glPushMatrix();
	glTranslatef(x, y, 0.0);
	glRotatef(angle, 0.0, 0.0, 1.0);
		draw_a_quad(0.0, 0.0, 0.1, 0.1);
	glPopMatrix();
	choosetex(0);
	return;
}

void
draw_arrow()
{
	int i;
	for(i = 0; i < check_count; i++) {
		glColor3fv(arrow_color[i]);
		show_arrow(check_str[i], tank0.posx + 0.15 * (i - check_count / 2), tank0.posy - 0.15); 
	}
	return;
}

void
show_under_line()
{
	glBegin(GL_TRIANGLES);
		glVertex2f(0.000, 0.000);
		glVertex2f(0.005, -0.005);
		glVertex2f(0.005, 0.005);
	glEnd();
	glBegin(GL_QUADS);
		glVertex2f(0.005, -0.005);
		glVertex2f(0.025, -0.005);
		glVertex2f(0.025, 0.005);
		glVertex2f(0.005, 0.005);
	glEnd();
	glBegin(GL_TRIANGLES);
		glVertex2f(0.025, -0.005);
		glVertex2f(0.030, 0.000);
		glVertex2f(0.025, 0.005);
	glEnd();
	return;
}

void
show_vertical_line()
{	
	glPushMatrix();
                glRotatef(90.0, 0.0, 0.0, 1.0);
                show_under_line();
        glPopMatrix();
	return;
}

void
show_zero()
{
		show_under_line();
		show_vertical_line();
	glPushMatrix();
		glTranslatef(0.00, 0.06, 0.00);
		show_under_line();
	glPopMatrix();
	glPushMatrix();
		glTranslatef(0.03, 0.00, 0.00);
		show_vertical_line();
		glTranslatef(0.00, 0.03, 0.00);
		show_vertical_line();
		glTranslatef(-0.03, 0.00, 0.00);
		show_vertical_line();
	glPopMatrix();
	return;
}

void
show_one()
{
	glPushMatrix();
		glTranslatef(0.03, 0.00, 0.00);
		show_vertical_line();
		glTranslatef(0.00, 0.03, 0.00);
		show_vertical_line();
	glPopMatrix();
        return;
}

void
show_two()
{
		show_vertical_line();
		show_under_line();
	glPushMatrix();
		glTranslatef(0.03, 0.03, 0.00);
		show_vertical_line();
	glPopMatrix();
	glPushMatrix();
		glTranslatef(0.00, 0.03, 0.00);
		show_under_line();
		glTranslatef(0.00, 0.03, 0.00);
		show_under_line();
	glPopMatrix();
        return;
}

void
show_three()
{
		show_under_line();
	glPushMatrix();
		glTranslatef(0.00, 0.03, 0.00);
		show_under_line();
		glTranslatef(0.00, 0.03, 0.00);
		show_under_line();
	glPopMatrix();
	glPushMatrix();
		glTranslatef(0.03, 0.00, 0.00);
		show_vertical_line();
		glTranslatef(0.00, 0.03, 0.03);
		show_vertical_line();
	glPopMatrix();
        return;
}

void
show_four()
{
	glPushMatrix();
		glTranslatef(0.03, 0.00, 0.00);
		show_vertical_line();
		glTranslatef(0.00, 0.03, 0.00);
		show_vertical_line();
		glTranslatef(-0.03, 0.00, 0.00);
		show_vertical_line();
	glPopMatrix();
	glPushMatrix();
		glTranslatef(0.00, 0.03, 0.00);
		show_under_line();
	glPopMatrix();
        return;
}

void
show_five()
{
	glPushMatrix();
		glTranslatef(0.03, 0.00, 0.00);
		glRotatef(180.0, 0.0, 1.0, 0.0);
		show_two();
	glPopMatrix();
        return;
}

void
show_six()
{
		show_under_line();
                show_vertical_line();
        glPushMatrix();
                glTranslatef(0.00, 0.03, 0.00);
                show_under_line();
                glTranslatef(0.00, 0.03, 0.00);
                show_under_line();
        glPopMatrix();
        glPushMatrix();
                glTranslatef(0.03, 0.00, 0.00);
                show_vertical_line();
                glTranslatef(-0.03, 0.03, 0.00);
                show_vertical_line();
        glPopMatrix();
	return;
}
void
show_seven()
{
	glPushMatrix();
		glTranslatef(0.00, 0.06, 0.00);
		show_under_line();
	glPopMatrix();
	show_one();
        return;
}

void
show_eight()
{
                show_under_line();
                show_vertical_line();
        glPushMatrix();
                glTranslatef(0.000, 0.030, 0.000);
                show_under_line();
		glTranslatef(0.000, 0.030, 0.000);
                show_under_line();
        glPopMatrix();
        glPushMatrix();
                glTranslatef(0.030, 0.000, 0.000);
                show_vertical_line();
                glTranslatef(0.000, 0.030, 0.000);
                show_vertical_line();
                glTranslatef(-0.030, 0.000, 0.000);
                show_vertical_line();
        glPopMatrix();
        return;
}

void
show_nine()
{
                show_under_line();
        glPushMatrix();
                glTranslatef(0.000, 0.030, 0.000);
                show_under_line();
                glTranslatef(0.000, 0.030, 0.000);
                show_under_line();
        glPopMatrix();
        glPushMatrix();
                glTranslatef(0.030, 0.000, 0.000);
                show_vertical_line();
                glTranslatef(0.000, 0.030, 0.000);
                show_vertical_line();
                glTranslatef(-0.030, 0.000, 0.000);
                show_vertical_line();
        glPopMatrix();
        return;
}

void
show_number(unsigned int num, float x, float y)
{
	glPushMatrix();
	glTranslatef(x, y, 0.0);
	switch(num) {
		case 0: show_zero(); break;
		case 1: show_one(); break;
		case 2: show_two(); break;
		case 3: show_three(); break;
		case 4: show_four(); break;
		case 5: show_five(); break;
		case 6: show_six(); break;
		case 7: show_seven(); break;
		case 8: show_eight(); break;
		case 9: show_nine(); break;
	}
	glPopMatrix();
	return;
}

void
show_point(float x, float y)
{
	glPushMatrix();
	glTranslatef(x, y, 0.0);
	glBegin(GL_QUADS);
		glVertex2f(0.00, 0.00);
		glVertex2f(-0.01, 0.00);
		glVertex2f(-0.01, -0.01);
		glVertex2f(0.00, -0.01);
	glEnd();
	glPopMatrix();
	return;
}

void
show_op(float x, float y)
{
	glPushMatrix();
		glTranslatef(x, y, 0);
		glPushMatrix();
			glTranslatef(0.00, 0.03, 0.00);
	 		show_under_line();
		glPopMatrix();
	glPopMatrix();
	return;
}

void
show_speed()
{
	int number;
	float speed = tank0.base_speed - tan(tank0.angle / 180.0 * 3.14159);
	if(speed >= 100.0 || speed <= -100.0) 
		return;
	if(speed < 0.0) { 
		show_op(eye_x + 0.75, eye_y - 0.90);
		speed = -speed;
	}
	number = (int)speed;
	show_number(number / 10, eye_x + 0.80, eye_y - 0.90);
	show_number(number % 10, eye_x + 0.85, eye_y - 0.90);
	show_point(eye_x + 0.90, eye_y - 0.90);
	number = (int)(speed * 10);
	number %= 10;
	show_number(number, eye_x + 0.91, eye_y - 0.90);
	glPushMatrix();
	glTranslatef(eye_x + 0.95, eye_y - 0.80, 0.0);
	glBegin(GL_QUADS);
		glColor3f(0.0, 1.0, 0.0);
		glVertex2f(0.00, 0.00);
		glVertex2f(0.01, 0.00);
		glVertex2f(0.01, tank0.base_speed / 100);
		glVertex2f(0.00, tank0.base_speed / 100);
	glEnd();
	glPopMatrix();
	return;
}

void
show_time()
{
        clock_gettime(CLOCK_REALTIME, &cur_time);
	if(start_flag) {
		if(net_flag) {
			send_req(GET_TIME);
			net_get(&all_time, sizeof(all_time));
		} else {
			clock_gettime(CLOCK_REALTIME, &cur_time);
			last_run_time = cur_time.tv_sec - start_time.tv_sec;
			all_time = run_time + last_run_time;
			if(all_time >= 1000)
				return;
		}
	} else {
		if(!net_flag) {
			run_time += last_run_time;
        		clock_gettime(CLOCK_REALTIME, &start_time);
       		 	last_run_time = 0;
		}
	}
	show_number((int)(all_time / 100), eye_x - 0.90, eye_y - 0.90);
	show_number((int)((all_time % 100) / 10), eye_x - 0.85, eye_y - 0.90);
	show_number((int)(all_time % 10), eye_x - 0.80, eye_y - 0.90);
	return;
}

void
show_sort()
{
	glColor3f(1.0, 0.0, 0.0);
	show_number(tank0.sort, eye_x - 0.90, eye_y - 0.80);
	return;
}

void
show_final_sort()
{
   	choosetex(sort_tex);
	glPushMatrix();
         	glTranslatef(tank0.posx, tank0.posy - 0.15, 0.0);
		draw_a_quad(0.0, 0.0, 0.2, 0.075);
     	glPopMatrix();
     	choosetex(0);
        show_number(tank0.sort, tank0.posx + 0.21, tank0.posy - 0.15);
	return;
}

void
show_wait()
{
	choosetex(wait_tex);
	glPushMatrix();
	glTranslatef(0.0, -0.15, 0.0);
	draw_a_quad(0.0, 0.0, 0.2, 0.1);
 	glPopMatrix();
   	choosetex(0);
	return;
}

void
show_tree(float x, float y)
{
	choosetex(tree_tex);
	glPushMatrix();
	glTranslatef(x, y, 0.0);
	draw_a_quad(0.0, 0.0, 0.4, 0.4);
	glPopMatrix();
	choosetex(0);
	return;
}

void
show_grass(float x, float y)
{
        glPushMatrix();
        glTranslatef(x, y, 0.0);
	draw_a_quad(0.0, 0.0, 0.2, 0.2);
        glPopMatrix();
	return;
}

void
rand_trees_and_grasses()
{
	int i;
	double angle;
	tree_count = (int)mapvertex[vertex_count-4];
	grass_count = (int)mapvertex[vertex_count-4] * 5;
	for(i = 0; i < tree_count; i++) {
		do {
			treepos[i].x = (rand() % (int)(mapvertex[vertex_count-4] * 10)) / 10.0;
			tank_pos(0.6, treepos[i].x, &treepos[i].y, &angle);
		} while(angle != 0.0);
	}
	for(i = 0; i < grass_count; i++) {
                do {
                        grasspos[i].x = (rand() % (int)(mapvertex[vertex_count-4] * 10)) / 10.0;
                        tank_pos(0.3, grasspos[i].x, &grasspos[i].y, &angle);
                } while(angle != 0.0);
        }
	return;
}

void
show_trees_and_grasses()
{
	int 		i;
	static	int	grass_point = 0;
	for(i = 0; i < tree_count; i++) {
		show_tree(treepos[i].x, treepos[i].y);
	}
        choosetex(upgrasstex[grass_point/50]);
        grass_point++;
        grass_point %= upgrasstex_count * 50;
	for(i = 0; i < grass_count; i++) {
		show_grass(grasspos[i].x, grasspos[i].y);
	}
	choosetex(0);
	return;
}

void
draw_box(int skill)
{
	float	lock = ((float)all_time - skills[skill].use_time) / skills[skill].cool_time / 10.0;
	if(lock > 0.1)
		lock = 0.1;
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_LINE_LOOP);
		glVertex2f(0.0, 0.0);
		glVertex2f(0.1, 0.0);
		glVertex2f(0.1, 0.1);
		glVertex2f(0.0, 0.1);
	glEnd();
	switch(skills[skill].skill_name) {
		case BOMB: 	choosetex(bomb_table_tex); break;
		case MISSILE: 	choosetex(missile_table_tex); break;
		case SPRINT:	choosetex(sprint_table_tex); break;
		case SHIELD:	choosetex(shield_table_tex); break;
	}
	draw_a_quad(0.0, 0.0, 0.1, 0.1);
	choosetex(0);
	glColor4f(0.4, 0.4, 0.4, 0.8);
	glBegin(GL_QUADS);
		glVertex2f(0.0, lock);
                glVertex2f(0.1, lock);
                glVertex2f(0.1, 0.1);
                glVertex2f(0.0, 0.1);
	glEnd();
	return;
}

void
show_skill_box()
{
	glPushMatrix();
	glTranslatef(eye_x + SKILL_BOX_POS_X, eye_y + SKILL_BOX_POS_Y, 0.0);
		draw_box(0);
	glTranslatef(0.1, 0.0, 0.0);
		draw_box(1);
	glTranslatef(0.1, 0.0, 0.0);
		draw_box(2);
	glTranslatef(0.1, 0.0, 0.0);
		draw_box(3);
	glPopMatrix();
	return;
}

int
show_welcom()
{
	static float process = 0.0;
	if(process == 1.0) {
		return 0;
	}
	process = loadtextures();
	choosetex(welcom_tex);
	glPushMatrix();
	glTranslatef(eye_x, eye_y, 0.0);
		draw_a_quad(-1.0, -1.0, 1.0, 1.0);
	choosetex(0);
	glTranslatef(-0.5, 0.0, 0.0);
		draw_a_quad(0.0, 0.0, process, 0.05);
	glPopMatrix();
	return 1;
}

void
show_player_num()
{
	show_number(tanks_count, eye_x + 0.85, eye_y - 0.80);
}
